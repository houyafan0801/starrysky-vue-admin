import request from '@/utils/request'

export function getMain() {
  return request({
    url: 'http://127.0.0.1:8000/api/home_page',
    method: 'get'
  })
}

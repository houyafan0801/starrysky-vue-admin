import request from '@/utils/request'
export function login(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: 'http://127.0.0.1:8000/api/info',
    method: 'get',
    headers:{ token },
    params: { token }
  })
}

export function logout() {
  return request({
    url: 'http://127.0.0.1:8000/api/logout',
    method: 'post'
  })
}

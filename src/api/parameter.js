import request from '@/utils/request'

export function getParameter(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/parameter',
    method: 'get',
    params
  })
}

export function deleteParameter(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/parameter',
    method: 'delete',
    params
  })
}

export function postParameter(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/parameter',
    method: 'post',
    data
  })
}

export function putParameter(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/parameter',
    method: 'put',
    data
  })
}

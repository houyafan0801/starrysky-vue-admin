import request from '@/utils/request'

export function getProject(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/project',
    method: 'get',
    params
  })
}

export function deleteProject(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/project',
    method: 'delete',
    params
  })
}

export function postProject(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/project',
    method: 'post',
    data
  })
}

export function putProject(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/project',
    method: 'put',
    data
  })
}

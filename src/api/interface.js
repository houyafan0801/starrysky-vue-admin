import request from '@/utils/request'

export function getInterface(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/interface',
    method: 'get',
    params
  })
}

export function deleteInterface(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/interface',
    method: 'delete',
    params
  })
}

export function postInterface(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/interface',
    method: 'post',
    data
  })
}

export function putInterface(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/interface',
    method: 'put',
    data
  })
}

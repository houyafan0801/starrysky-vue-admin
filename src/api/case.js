import request from '@/utils/request'

export function getCase(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/case',
    method: 'get',
    params
  })
}

export function deleteCase(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/case',
    method: 'delete',
    params
  })
}

export function postCase(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/case',
    method: 'post',
    data
  })
}

export function putCase(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/case',
    method: 'put',
    data
  })
}

export function runCase(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/run',
    method: 'post',
    data
  })
}

export function caseReport(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/case_report',
    method: 'get',
    params
  })
}

export function getRelyCase(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/get_rely_case',
    method: 'get',
    params
  })
}

export function joinCase(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/join_case',
    method: 'get',
    params
  })
}


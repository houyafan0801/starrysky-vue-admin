import request from '@/utils/request'

export function getCollectionReport(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/collection_report',
    method: 'get',
    params
  })
}

export function getReport(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/report',
    method: 'get',
    params
  })
}

export function postJoinCase(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/join_case',
    method: 'post',
    data
  })
}

export function getCaseCollection(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/case_collection',
    method: 'get',
    params
  })
}

export function getJoinCase(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/join_case',
    method: 'get',
    params
  })
}

export function deleteCaseCollection(params) {
  return request({
    url: 'http://127.0.0.1:8000/api/case_collection',
    method: 'delete',
    params
  })
}

export function postRunCollection(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/run_collection',
    method: 'post',
    data
  })
}

export function postCaseCollection(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/case_collection',
    method: 'post',
    data
  })
}

export function putCaseCollection(data) {
  return request({
    url: 'http://127.0.0.1:8000/api/case_collection',
    method: 'put',
    data
  })
}

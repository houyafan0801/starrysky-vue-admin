/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const projectRouter = {
  path: '/project',
  component: Layout,
  redirect: 'noRedirect',
  meta: {
    icon: 'lock',
    roles: ['qa'] // you can set roles in root nav
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/project/index'),
      name: '项目管理',
      meta: { title: '项目管理', icon: 'table' }
    }
  ]
}

const parameterRouter = {
  path: '/parameter',
  component: Layout,
  redirect: 'noRedirect',
  meta: {
    icon: 'lock',
    roles: ['qa'] // you can set roles in root nav
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/parameter/index'),
      name: '参数管理',
      meta: { title: '参数管理', icon: 'table' }
    }
  ]
}
const mainRouter = {
  path: '/main',
  component: Layout,
  redirect: 'noRedirect',
  meta: {
    icon: 'lock',
    roles: ['qa'] // you can set roles in root nav
  },
  children: [
    {
      path: 'mian',
      component: () => import('@/views/api-main/index'),
      name: '首页',
      meta: { title: '首页', icon: 'table' }
    }
  ]
}

const interfaceRouter = {
  path: '/interface',
  component: Layout,
  redirect: 'noRedirect',
  meta: {
    icon: 'lock',
    roles: ['qa'] // you can set roles in root nav
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/interface/index'),
      name: '接口管理',
      meta: { title: '接口管理', icon: 'table' }
    }
  ]
}

const caseRouter = {
  path: '/case',
  component: Layout,
  redirect: 'noRedirect',
  meta: {
    icon: 'lock',
    roles: ['qa'] // you can set roles in root nav
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/case/index'),
      name: '用例管理',
      meta: { title: '用例管理', icon: 'table' }
    }
  ]
}
const collectionRouter = {
  path: '/collection',
  component: Layout,
  redirect: 'noRedirect',
  meta: {
    icon: 'lock',
    roles: ['qa'] // you can set roles in root nav
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/collection/index'),
      name: '用例集合',
      meta: { title: '用例集合', icon: 'table' }
    }
  ]
}
const reportRouter = {
  path: '/report',
  component: Layout,
  redirect: 'noRedirect',
  meta: {
    icon: 'lock',
    roles: ['qa'] // you can set roles in root nav
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/report/index'),
      name: '用例报告',
      meta: { title: '用例报告', icon: 'table' }
    }
  ]
}
export { parameterRouter, projectRouter, mainRouter, interfaceRouter, caseRouter, collectionRouter, reportRouter }

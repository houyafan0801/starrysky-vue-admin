import { getProject, deleteProject, postProject, putProject } from '@/api/project'
function data() {
  return {
    currentPage: 1,
    total: 0,
    filter_query: {
      search: undefined,
      limit: 10,
      page: 1
    },
    tableData: [],
    dialogFormVisible: false,
    dialog_title: '添加',
    form: {
      name: '',
      host: '',
      desc: '',
      user: ''
    },
    formLabelWidth: '120px',
    case_data: [],
    value: [],
    loading: false,
    button_loading: false,
    id: undefined
  }
}

function methods() {
  return {
    handleSizeChange(val) {
      this.filter_query.limit = val
      this.filter_query.page = 1
      this.get_page_data()
    },
    handleCurrentChange(val) {
      this.filter_query.page = val
      this.get_page_data()
    },
    data_to_string(data) {
      const tmp = new FormData()
      for (var key in data) {
        tmp.append(key, data[key])
      }
      return tmp
    },
    get_page_data: function() {
      this.loading = true //
      getProject(this.filter_query).then((response) => {
        this.tableData = response.data.data
        console.log(response.data.count)
        this.total = response.data.count
        this.loading = false //
      })
    },
    search: function() {
      this.filter_query.page = 1
      this.get_page_data()
    },
    handle_edit(row) {
      this.form = Object.assign({}, { name: row.name, host: row.host, desc: row.desc, user: this.$store.getters.user }) // 深拷贝
      this.form.id = row.id
      this.dialog_title = '编辑'
      this.dialogFormVisible = true // 打开模态框
    },
    handle_delete(row) {
      this.$confirm('确认删除当前项目吗？', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning',
        beforeClose: (action, instance, done) => {
          if (action === 'confirm') {
            instance.confirmButtonLoading = true
            instance.confirmButtonText = '删除中...'
            deleteProject({ id: row.id }).then((response) => {
              instance.confirmButtonLoading = false
              done()
              this.$message({
                type: 'success',
                message: '删除成功'
              })
              this.get_page_data()
            }).catch((error) => {
              this.$message({
                type: 'error',
                message: error
              })
            })
          } else {
            done()
          }
        }
      }).then(() => {
      }).catch(() => {
        this.$message({
          type: 'info',
          message: '取消删除'
        })
      })
    },
    add_collection() {
      this.reset_form()
      this.dialog_title = '添加'
      this.dialogFormVisible = true
    },
    reset_form() {
      // form数据置为空
      this.form = {
        name: '',
        desc: '',
        host: '',
        user: this.$store.getters.user
      }
    },
    operate() {
      this.button_loading = true
      if (this.dialog_title === '添加') {
        postProject(this.data_to_string(this.form)).then((response) => {
          this.button_loading = false
          if (response.data.code === 0) {
            this.dialogFormVisible = false // 关闭模态框
            this.get_page_data()
          } else {
            this.$message({
              type: 'error',
              message: response.data.msg
            })
          }
        }).catch((error) => {
          this.button_loading = false
          console.log(error)
        })
      } else if (this.dialog_title === '编辑') {
        putProject(this.data_to_string(this.form)).then((response) => {
          this.button_loading = false
          if (response.data.code === 0) {
            this.dialogFormVisible = false // 关闭模态框
            this.get_page_data()
          } else {
            this.$message({
              type: 'error',
              message: response.data.msg
            })
          }
        }).catch((error) => {
          this.button_loading = false
          console.log(error)
        })
      }
    }
  }
}

export {
  methods, data
}

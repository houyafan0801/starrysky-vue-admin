export function data_to_string(data) {
  const tmp = new FormData()
  for (var key in data) {
    tmp.append(key, data[key])
  }
  return tmp
}
